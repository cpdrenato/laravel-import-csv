<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\CsvFileController;

Route::get('/', function () {
    return view('welcome');
});

Route::get('index', 'CsvFileController@index');
Route::post('upload', 'CsvFileController@upload');
Route::post('import', 'CsvFileController@import');

Route::get('startTime', 'CenvertedTimeController@index');

Route::get('pdfview',array('as'=>'pdfview','uses'=>'ItemController@pdfview'));
